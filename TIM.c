
#include "TIM.h"
void TIM_Init(void){
	/*TIM0作为时间定时器*/
	//设置分频数
	TIM0->PS_b.PS = 1;							 /* set prescaler (timer clk = FSYS/(PS+1)) */
	TIM0->AR_b.AR = TIM0_AR;						/* set period (timer period = (AR+1)/timer clk) */
	//定时器周期等于((分频+1)*(自动装载值+1))/总频=0.01ms
	TIM0->IEN = 0;								/* disable all interrupts */
	TIM0->CR_b.CM = 0;						/* set clock multiplexer; 0: internal clock (fsys = 20 MHz), 1: external clock (TIM0_IN) */
	TIM0->CR_b.MOD = 0;						/* set mode; 0: timer, 1: compare, 2: capture */
	TIM0->CR_b.UIM = 0;						/* set update interrupt mode; 0: IRQ on overflow||update, 1: IRQ on overflow */
	TIM0->CR_b.UEM = 0;						/* set update event mode; 0: update on overflow||TU, 1: update on TU	*/
	TIM0->CR_b.EN = 1;						/* enable timer */	
	
	NVIC_SetPriority(TIM0_IRQn, TIM0_IRQ_PRIO);
	TIM0->IEN_b.UPD=1;						//开启更新中断
	TIM0->EIPND_b.UPD=1;					
	NVIC_EnableIRQ(TIM0_IRQn);;		/* enable in NVIC */
	
	/*TIM1作为步进电机的最后一相PWM输出*/
	//设置分频数
	TIM1->PS_b.PS = 1;							 /* set prescaler (timer clk = FSYS/(PS+1)) */
	TIM1->AR_b.AR = 1000-1;						/* set period (timer period = (AR+1)/timer clk) */
	//定时器周期等于(分频+1*自动装载值+1)/总频=1ms
	TIM1->CR_b.OCM=2;
	/*
	Output Control Mode
	0 Constant at ‘0’
	1 Constant at ‘1’
	2 PWM signal
	3 Inverted PWM signal
	4 TIMERx_IN_FILT signal
	5 Inverted TIMERx_IN_FILT signal
	6 reserved
	7 reserved
	*/
	TIM1->CC_b.CC=999;
	
	TIM1->IEN = 0;							/* disable all interrupts */
	TIM1->CR_b.CM = 0;						/* set clock multiplexer; 0: internal clock (fsys = 20 MHz), 1: external clock (TIM0_IN) */
	TIM1->CR_b.MOD = 1;						/* set mode; 0: timer, 1: compare, 2: capture */
	TIM1->CR_b.UIM = 0;						/* set update interrupt mode; 0: IRQ on overflow||update, 1: IRQ on overflow */
	TIM1->CR_b.UEM = 0;						/* set update event mode; 0: update on overflow||TU, 1: update on TU	*/
	TIM1->CR_b.CCBE=1;
	TIM1->CR_b.CCU=0;
	TIM1->IEN_b.CMP=1;
	TIM1->CR_b.EN = 1;						/* enable timer */	
	
}

int TIM0_Handler_Counter=0;

void TIM0_IRQHandler(void){
	u32 pending_u32;
	pending_u32 = TIM0->EIPND&0xFF;			/* read enabled interrupts pending flag TIM0  */
	TIM0->EIPND = pending_u32;				/* clear enabled interrupts pending flags TIM0 */
	if(pending_u32&0x01)						/* if EOP1 flag is set */
	{	
		TIM0_Handler_Counter++;
	}
}

int TIM1_Handler_Counter=0;

void TIM1_IRQHandler(void){
	u32 pending_u32;
	pending_u32 = TIM1->EIPND&0xFF;			
	TIM1->EIPND = pending_u32;				
	if(pending_u32&0x02)						
	{	
		TIM1_Handler_Counter++;
	}
}

