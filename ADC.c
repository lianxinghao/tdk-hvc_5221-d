
#include "ADC.h"
#include "EPWM.h"
#include "TIM.h"
#include "main.h"
#include "Variable.h"
#include "math.h"
#include "EEPROM.h"
/*  */
struct ADC_Raw_Value ADC_Raw_Value;	/* 原始数据 */
struct ADC_Value ADC_Voltage;		/* 处理数据 */
extern void SafeEnaADC(void);

int ADC_Handler_Counter=0;

int Motor_State_Open_Real=-1;
int Motor_State_Close_Real=1;
int Last_Motor_State_Open_Real=-1;
int Last_Motor_State_Close_Real=1;
int Open_Really_All_Step=0;
int Close_Really_All_Step=0;
#define Open_Judge_Value 2047
#define Close_Judge_Value 2047
int Max_Data=2047;
int Last_DIR;

int Last_Motor_Current=0,ADD_Motor_Current=0;
/*  */

void ADC_Init(void){
	ADC->CR_b.HPTS = 4; 			/* trigger source 0=SW, 1=EPWM0, 2=EPWM1, 3=EPWM2, 4=trigger timer, 5=EPWM0|EPWM1|EPWM2 */
	ADC->CR_b.LPTS = 4; 			/* trigger source 0=SW, 1=EPWM0, 2=EPWM1, 3=EPWM2, 4=trigger timer, 5=EPWM0|EPWM1|EPWM2 */
	
	/* queue source 0=AVSS, 1=BVDD, 2=MOUT3, 3=MOUT2, 4=MOUT1, 5=MOUT0, 6=MVSS0, 7=MVSS1, 8=RS0, 9=RS0ref, 10=RS1, 11=RS1ref, 12=TEMPSENSOR.. */
	/* PGA gain 0=1, 1=2, 2=4, 3=8, 4=16 (setting ignored for TEMP) */
	
	ADC->QCR1_b.Q0SEL 	= 1; //ADC采集队列获取BVDD电流值
	ADC->QCR1_b.Q0G 	= 2;
	
	ADC->QCR1_b.Q1SEL 	= 8; //ADC采集队列获取RS0电流值
	ADC->QCR1_b.Q1G 	= 0;
	
	ADC->QCR1_b.Q2SEL 	= 2; //ADC采集队列获取MOUT3的电压值
	ADC->QCR1_b.Q2G 	= 2;
	
	ADC->QCR1_b.Q3SEL 	= 3; //ADC采集队列获取MOUT2的电压值
	ADC->QCR1_b.Q3G 	= 2;
	
	ADC->QCR2_b.Q4SEL 	= 4; //ADC采集队列获取MOUT1的电压值
	ADC->QCR2_b.Q4G 	= 2;
	
	ADC->QCR2_b.Q5SEL 	= 5; //ADC采集队列获取MOUT0的电压值
	ADC->QCR2_b.Q5G 	= 2;
	
	ADC->QCR2_b.Q6SEL 	= 11; //ADC采集队列获取温度传感器的电压值
	ADC->QCR2_b.Q6G 	= 2;
	
	ADC->QCR2_b.Q7SEL 	= 10; //ADC采集队列获取RS1电流值
	ADC->QCR2_b.Q7G 	= 0;
	//开启传输中断
	
	ADC->IEN_b.HPEOC = 1;				/* enable end of conversion interrupt */
	ADC->IEN_b.LPEOC = 1;				/* enable end of conversion interrupt */
	
	//开启ADC中断，设置中断优先级
	NVIC_SetPriority(ADC_IRQn, ADC_IRQ_PRIO);	/* set ADC_IRQ_PRIO priority */
	NVIC_EnableIRQ(ADC_IRQn);	
	
	ADC->CR_b.NHP = 0;					//队列长度，HP=NHP+1，LP=7-HNP
	
	ADC->CR_b.EN = 1;					/* ADC enable */
	SafeEnaADC();
	
	ADC->TTP = 99; 					//采样周期=20*(TTP+1)/(FSYS)=0.1ms
	ADC->CR_b.TTEN =1; 					//定时触发使能	
}

/**************************************
	ADC ISR
	ADC中断内获取采集队列数据
	同时直接对数据进行处理
	有可能不能放在中断中，是中断影响了实际数据
	做堵转检测
	ad基本位数为12，PGA放大系数
**************************************/

void ADC_IRQHandler (void)
{
	unsigned int pending;
	pending = (ADC->EIPND);		/* read enabled interrupts pending flags  */
	ADC->EIPND = pending;			/* clear enabled interrupts pending flags */
	
	if(pending&BIT_4) {				
		Raw_Data_Get();		//获取原始数据
		Voltage_Process();	//电压处理
		Motor_Position_Judge();	//得到电机位置
		Current_Process();	//电流处理
		Motor_Step_Count();	//电机步数统计
		ERROR_CHACK();		//错误检测
		ADC_Handler_Counter++;
	}
	
}	
/*
	恢复所有统计变量
*/

void Value_Recover(void)
{
	Save_Beat=0;
	Last_Save_Beat=0;
	MOTOR_DIR=0;
	Last_MOTOR_DIR=-1;
	Change_Eight_Beat_Counter=0;
	Time=Start_Speed;
	Mos_State[0]=0;
	Mos_State[1]=0;
	Mos_State[2]=0;
	Mos_State[3]=0;
	Mos_State[4]=0;
	Mos_State[5]=0;
	Mos_State[6]=0;
	Mos_State[7]=0;
	Variable_Init();
}
/*
	MOS是否全部关闭判断
*/
bool Mos_CLOSE_Judge(void)
{
	for(int i=0;i<8;i++){
		if(Mos_State[i]==1){
			return 0;
		}
	}
	return 1;
}
/*
	获取原始数据
*/
void Raw_Data_Get(void)
{
	ADC_Raw_Value.Raw_BVDD=ADC->DR0_b.DATA;
	ADC_Raw_Value.Raw_RS0=ADC->DR1_b.DATA;
	ADC_Raw_Value.Raw_MOUT0=ADC->DR2_b.DATA;
	ADC_Raw_Value.Raw_MOUT1=ADC->DR3_b.DATA;
	ADC_Raw_Value.Raw_MOUT2=ADC->DR4_b.DATA;
	ADC_Raw_Value.Raw_MOUT3=ADC->DR5_b.DATA;
	ADC_Raw_Value.Raw_TEMPERATURE=ADC->DR6_b.DATA;
	ADC_Raw_Value.Raw_RS1=ADC->DR7_b.DATA;	
	
}
/*
	RS01电流处理、统计
	根据不同的MOS开关状态读取处理电流
*/
void Current_Process(void)
{
	/* adc采集电流处理 */
	if(ADC_Raw_Value.Raw_RS0>ADC_CURRENT_MID_VALUE){
		ADC_Voltage.ADC_Queue_RS0_Current=0;
	}else{
		ADC_Voltage.ADC_Queue_RS0_Current=ADC_Raw_Value.Raw_RS0;
	}
	if(ADC_Raw_Value.Raw_RS1>ADC_CURRENT_MID_VALUE){
		ADC_Voltage.ADC_Queue_RS1_Current=0;
	}else{
		ADC_Voltage.ADC_Queue_RS1_Current=ADC_Raw_Value.Raw_RS1;
	}
	ADC_Voltage.ADC_Queue_RS_Current=ADC_Voltage.ADC_Queue_RS0_Current+ADC_Voltage.ADC_Queue_RS1_Current;
	/* 判断电机位置 */
	if(ADC_Voltage.ADC_Queue_RS0_Current==0){
		RS0_CURRENT_POSITION=0;
	}else{
		if(ADC_Voltage.ADC_Queue_RS0_Current>30)RS0_CURRENT_POSITION=1;
	}
	if(ADC_Voltage.ADC_Queue_RS1_Current==0){
		RS1_CURRENT_POSITION=0;
	}else{
		if(ADC_Voltage.ADC_Queue_RS1_Current>30)RS1_CURRENT_POSITION=1;
	}
	/* 计算一整个周期电流,应该对RS0-1就行分开计算电流,只能计算稳速转动时的电流 */
	if(RS0_CURRENT_POSITION==0&&RS0_CURRENT_POSITION_LAST==1)
	{
		RS0_CURRENT_CALCULATE_FLAG++;
	}
	if(RS1_CURRENT_POSITION==0&&RS1_CURRENT_POSITION_LAST==1)
	{
		RS1_CURRENT_CALCULATE_FLAG++;
	}
	switch(RS0_CURRENT_CALCULATE_FLAG)
	{
		case 1:
			RS0_All_CURRENT+=ADC_Voltage.ADC_Queue_RS0_Current;	/* 总电流统计 */
			RS0_AVERAGE_COUNT++;
			break;
		case 2:
			ADC_Voltage.RS0_Average=RS0_All_CURRENT/(RS0_AVERAGE_COUNT);
			RS0_All_CURRENT=0;
			RS0_AVERAGE_COUNT=0;
			RS0_CURRENT_CALCULATE_FLAG--;
			break;
	}
	switch(RS1_CURRENT_CALCULATE_FLAG)
	{
		case 1:
			RS1_All_CURRENT+=ADC_Voltage.ADC_Queue_RS1_Current;	/* 总电流统计 */
			RS1_AVERAGE_COUNT++;
			break;
		case 2:
			ADC_Voltage.RS1_Average=RS1_All_CURRENT/(RS1_AVERAGE_COUNT);
			RS1_All_CURRENT=0;
			RS1_AVERAGE_COUNT=0;
			RS1_CURRENT_CALCULATE_FLAG--;
			break;
	}
	ADC_Voltage.RS_Average=ADC_Voltage.RS0_Average+ADC_Voltage.RS1_Average;
	ADC_Voltage.Motor_Current=ADC_Voltage.RS_Average*ADC_RATIO;	/* 真实电流值ma */
	/* 计算平均电流 */
	Real_Current_Count+=ADC_Voltage.Motor_Current;
	Real_Count++;
	if(Real_Count==Simple_Number){
		ADC_Voltage.Motor_Current_Average=Real_Current_Count/Simple_Number;
		Real_Count=0;
		Real_Current_Count=0;
	}
	/* 从平均电流中识别最大速度电流 */
	
	/* 刷新上次电流判断位置 */
	RS0_CURRENT_POSITION_LAST=RS0_CURRENT_POSITION;	
	RS1_CURRENT_POSITION_LAST=RS1_CURRENT_POSITION;	
}

/*
	MOUT电压处理限幅
*/
void Voltage_Process(void)
{
	ADC_Voltage.ADC_Queue_AVSS_Current=ADC->DR0_b.DATA;
	
	if(ADC->DR2_b.DATA>Max_Data){
		ADC_Voltage.ADC_Queue_MOUT3_Voltage=0;
	}else{
		ADC_Voltage.ADC_Queue_MOUT3_Voltage=ADC->DR2_b.DATA;
	}
	
	if(ADC->DR3_b.DATA>Max_Data){
		ADC_Voltage.ADC_Queue_MOUT2_Voltage=0;
	}else{
		ADC_Voltage.ADC_Queue_MOUT2_Voltage=ADC->DR3_b.DATA;
	}
	
	if(ADC->DR4_b.DATA>Max_Data){
		ADC_Voltage.ADC_Queue_MOUT1_Voltage=0;
	}else{
		ADC_Voltage.ADC_Queue_MOUT1_Voltage=ADC->DR4_b.DATA;
	}
	
	if(ADC->DR5_b.DATA>Max_Data){
		ADC_Voltage.ADC_Queue_MOUT0_Voltage=0;
	}else{
		ADC_Voltage.ADC_Queue_MOUT0_Voltage=ADC->DR5_b.DATA;
	}
	
	//温度数据处理部分
	ADC_Voltage.ADC_Queue_TMP_SENSOR_Voltage=ADC->DR6_b.DATA;
	//ADC_Data_Processed.Temperature=(ADC_Voltage.ADC_Queue_TMP_SENSOR_Voltage-899.6)/3.191;
}
/*
	通过采样电流判断电机当前位置
*/
void Motor_Position_Judge(void)
{
			/*
	获取电机当前所处8拍的位置
	对计步数的处理还是过于简单
	adc采样时机
	Motor_State_Real变量不能有过多的赋值，免得在jscope采样时出现中间量
	*/
	
	if((ADC_Voltage.ADC_Queue_MOUT0_Voltage>ADC_Voltage.ADC_Queue_MOUT1_Voltage)&&((ADC_Voltage.ADC_Queue_MOUT0_Voltage>=Open_Judge_Value&&Dir_Flag==1)||(ADC_Voltage.ADC_Queue_MOUT0_Voltage>=Close_Judge_Value&&Dir_Flag==0))){
		if((ADC_Voltage.ADC_Queue_MOUT2_Voltage>ADC_Voltage.ADC_Queue_MOUT3_Voltage)&&((ADC_Voltage.ADC_Queue_MOUT2_Voltage>=Open_Judge_Value&&Dir_Flag==1)||(ADC_Voltage.ADC_Queue_MOUT2_Voltage>=Close_Judge_Value&&Dir_Flag==0))){
			Motor_State_Open_Real=1;
			Motor_State_Close_Real=1;
		}else if((ADC_Voltage.ADC_Queue_MOUT3_Voltage>ADC_Voltage.ADC_Queue_MOUT2_Voltage)&&((ADC_Voltage.ADC_Queue_MOUT3_Voltage>=Open_Judge_Value&&Dir_Flag==1)||(ADC_Voltage.ADC_Queue_MOUT3_Voltage>=Close_Judge_Value&&Dir_Flag==0))){
			Motor_State_Open_Real=7;
			Motor_State_Close_Real=7;
		}else{
			Motor_State_Open_Real=0;
			Motor_State_Close_Real=0;
		}
	}else if((ADC_Voltage.ADC_Queue_MOUT1_Voltage>ADC_Voltage.ADC_Queue_MOUT0_Voltage)&&((ADC_Voltage.ADC_Queue_MOUT1_Voltage>=Open_Judge_Value&&Dir_Flag==1)||(ADC_Voltage.ADC_Queue_MOUT1_Voltage>=Close_Judge_Value&&Dir_Flag==0))){
		if((ADC_Voltage.ADC_Queue_MOUT2_Voltage>ADC_Voltage.ADC_Queue_MOUT3_Voltage)&&((ADC_Voltage.ADC_Queue_MOUT2_Voltage>=Open_Judge_Value&&Dir_Flag==1)||(ADC_Voltage.ADC_Queue_MOUT2_Voltage>=Close_Judge_Value&&Dir_Flag==0))){
			Motor_State_Open_Real=3;
			Motor_State_Close_Real=3;
		}else if((ADC_Voltage.ADC_Queue_MOUT3_Voltage>ADC_Voltage.ADC_Queue_MOUT2_Voltage)&&((ADC_Voltage.ADC_Queue_MOUT3_Voltage>=Open_Judge_Value&&Dir_Flag==1)||(ADC_Voltage.ADC_Queue_MOUT3_Voltage>=Close_Judge_Value&&Dir_Flag==0))){
			Motor_State_Open_Real=5;
			Motor_State_Close_Real=5;
		}else{
			Motor_State_Open_Real=4;
			Motor_State_Close_Real=4;
		}
	}else{
		if((ADC_Voltage.ADC_Queue_MOUT2_Voltage>ADC_Voltage.ADC_Queue_MOUT3_Voltage)&&((ADC_Voltage.ADC_Queue_MOUT2_Voltage>=Open_Judge_Value&&Dir_Flag==1)||(ADC_Voltage.ADC_Queue_MOUT2_Voltage>=Close_Judge_Value&&Dir_Flag==0))){
			Motor_State_Open_Real=2;
			Motor_State_Close_Real=2;
		}else if((ADC_Voltage.ADC_Queue_MOUT3_Voltage>ADC_Voltage.ADC_Queue_MOUT2_Voltage)&&((ADC_Voltage.ADC_Queue_MOUT3_Voltage>=Open_Judge_Value&&Dir_Flag==1)||(ADC_Voltage.ADC_Queue_MOUT3_Voltage>=Close_Judge_Value&&Dir_Flag==0))){
			Motor_State_Open_Real=6;
			Motor_State_Close_Real=6;
		}else{
			Motor_State_Open_Real=-1;
			Motor_State_Close_Real=1;
		}
	}
}
/*
	通过当前电机状态与之前电机状态计算电机走过的步数
	1、通过第一次堵转找到0位置点，开始计步
	2、第二次堵转找到记录总步数，停止计步
	
*/
void Motor_Step_Count(void)
{
	/* 电机运行步数统计 */
	if(LOCKED_ROTOR_ERROR!=1)
	{
		if(MOTOR_DIR!=Last_DIR){
			if((MOTOR_DIR-1)%8==Last_DIR){
				Close_Really_All_Step++;//确保没有失步与跳步计数
			}
			if((MOTOR_DIR+1)%8==Last_DIR){
				Open_Really_All_Step++;//确保没有失步与跳步计数
			}
		}
		Last_DIR=MOTOR_DIR;
	}else
	{
		if(LOCKED_ROTOR_ERROR_COUNT==1)
		{
			Close_Really_All_Step=0;
			Open_Really_All_Step=0;
		}else 
		if(LOCKED_ROTOR_ERROR_COUNT==2)
		{
			INIT_ALL_STEP=Close_Really_All_Step;
		}
			
	}
}
/*
	故障检测：过流故障、开路故障、堵转故障
	堵转检测使用短时间内大电流的增幅作为判断标准，虽然减速也会导致大幅度的电流增幅，所以在做减速时应该减缓速度变换速率，保证堵转检测的准确性
	目前只需要能够在最大转速下能够做好堵转检测就行，当time==100时的判断正确就可，因为如果一开始就堵转的话，那么就不会出现电流突变
*/
void ERROR_CHACK(void)
{
	
//		/* 故障检测 */
//		/* 过流故障发生时，停止电机、停止旋转函数、恢复中间变量 */
//		if(ADC_Voltage.Motor_Current>OVER_CURRENT_LIMIT&&OVER_CURRENT_ERROR==0&&Rotate_Finish==0){
//			Set_Mos_State(0,0,0,0,0,0,0,0);/* 出现过流停止电机 */
//			OVER_CURRENT_ERROR=1;	/* 设置故障标志位 */
//			ERROR_HAPPENED_TIME=TIM0_Handler_Counter;/* 纪录故障发生TIM0时间 */
//			Value_Recover();//恢复变量
//		}
//		/* 开路故障发生时，停止电机、停止旋转函数、恢复中间变量 */
//		if(ADC_Voltage.Motor_Current<OPEN_CIRCLE_LIMIT&&OPEN_CIRCLE_ERROR==0&&(!Mos_CLOSE_Judge())&&Rotate_Finish==0){
//			Set_Mos_State(0,0,0,0,0,0,0,0);/* 出现开路停止电机 */
//			OPEN_CIRCLE_ERROR=1;
//			ERROR_HAPPENED_TIME=TIM0_Handler_Counter;/* 纪录故障发生TIM0时间 */
//			Value_Recover();//恢复变量
//		}
	
	
//		/* 堵转检测:1.进入匀速转动时检测堵转2.启动过程中堵转3.一开始就堵转 */
	if(OUT_CONTROL_FLAG==0)
	{
		if(Time==100&&LOCKED_ROTOR_ERROR!=1){
			if(LOCKED_MOTOR_DETECT_RUNING()==1)
			{
				LOCKED_ROTOR_ERROR=1;	/* 实际电流与预期电流不符，判定堵转发生 */
				LOCKED_ROTOR_ERROR_COUNT++;
				Set_Mos_State(0,0,0,0,0,0,0,0);		
				Value_Recover();
//				EEPROM_WEITE_FLAG=EEPROM_WRITE();	/* 将堵转标志写入EEPROM */
			}
			else if(MAX_START_CURRENT!=0)
			{
				if(fabs(MAX_START_CURRENT-ADC_Voltage.Motor_Current_Average)<100)//发生在启动了一会电机电流已经小于堵转电流的情况
				{
					LOCKED_ROTOR_ERROR=1;	/* 实际电流与预期电流不符，判定堵转发生 */
					LOCKED_ROTOR_ERROR_COUNT++;
					Set_Mos_State(0,0,0,0,0,0,0,0);		
					Value_Recover();
//					EEPROM_WEITE_FLAG=EEPROM_WRITE();	/* 将堵转标志写入EEPROM */
				}
				MAX_START_CURRENT=0;
			}
		}else if(Time>100)/*2.启动时堵转,出现Time减少电流不变得情况就是初始堵转*/
		{
			if(ADC_Voltage.Motor_Current_Average>MAX_START_CURRENT)MAX_START_CURRENT=ADC_Voltage.Motor_Current_Average;
		}
	}
}
/*
	运行时：堵转检测用突变电流判断，当电流增量高于0.1A左右就认为发生堵转。
	开始时：记录最大电流，对比当Time==100时，差值超过认为堵转
*/
int LOCKED_MOTOR_DETECT_RUNING(void)
{
	if(Current_queue.size==10)
	{
		Current_queue.queue[Current_queue.back%11]=ADC_Voltage.Motor_Current_Average;
		Current_Sudden_Change=Current_queue.queue[Current_queue.back%11]-Current_queue.queue[Current_queue.head%11];
		Current_queue.back++;
		Current_queue.head++;
		if(Current_Sudden_Change>80)return 1;
	}else
	{
		Current_queue.queue[Current_queue.back%11]=ADC_Voltage.Motor_Current_Average;
		Current_queue.back++;
		Current_queue.size++;
	}
	return 0;
}


