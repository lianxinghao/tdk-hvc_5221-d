#include "Variable.h"
/*
	故障检测部分变量
*/
bool OVER_CURRENT_ERROR=0;		/* 过流故障标志位 */
bool OPEN_CIRCLE_ERROR=0;		/* 开路故障标志位 */
bool LOCKED_ROTOR_ERROR=0;		/*堵转故障标志位 */
int ERROR_HAPPENED_TIME=0;		/* 故障发生时间 */
unsigned int LOCKED_ROTOR_ERROR_COUNT=0;	/*堵转故障发生次数 */

bool MOS_STATE_CHANGE=0;		/* MOS状态更改标志 */



/*
	电流检测部分变量
*/
int ADC_CURRENT_MID_VALUE=32768;
int ADC_CURRENT_MAX_VALUE=65536;
int RS0_CURRENT_POSITION=-1;		/* 电流矢量位置 */
int RS0_CURRENT_POSITION_LAST=-1;	/* 上次电流矢量位置 */
int RS1_CURRENT_POSITION=-1;		/* 电流矢量位置 */
int RS1_CURRENT_POSITION_LAST=-1;	/* 上次电流矢量位置 */

/* 
电流状态：1表示有电流通过
0:10
1:11
2:01
3:00
*/

int RS0_AVERAGE_COUNT=0;
int RS1_AVERAGE_COUNT=0;
int	RS0_All_CURRENT=0;
int	RS1_All_CURRENT=0;
int RS0_CURRENT_CALCULATE_FLAG=0;	/* 电流开始计算标志位 */
int RS1_CURRENT_CALCULATE_FLAG=0;	/* 电流开始计算标志位 */
/*
	堵转部分变量
*/
double ADC_RATIO=6.70;				/* 电流比例估算值 */
double Motor_Locked_Current=500;	/* 堵转电流 */
double Current_Judge_Gap=50;		/* 电流偏差 */
double Motor_Max_Speed_Current=380;	/* 最大转速电流,这个值不能固定，应该去识别 */
double Real_Current_Count=0;
int Simple_Number=500;					/* 采样数量点 */
int Real_Count=0;

struct Queue Current_queue;			/* 电流队列 */
double Current_Sudden_Change;		/* 电流突变量 */
double MAX_START_CURRENT=0;
double MIN_START_CURRENT=65535;
/*
	EPWM.C
*/
int Eight_Beat_Time=10;//单位0.1ms，转动频率，1ms一步
int Save_Beat=0;   //当前步数统计
int Change_Eight_Beat_Counter=0;

int Last_MOTOR_DIR=-1;
int Dir_Flag=0;	/* 正反转方向标志 */
int MOTOR_DIR=0;
int Mos_State[8];
int ERROR_RECOVER=0;
int Rotate_Finish=0;

int Last_Save_Beat=0;
int Time=Start_Speed;

int CLOSE_EXV_FLAG=0;
int OPEN_EXV_FLAG=0;
int EXV_STEP=0;
int INIT_ALL_STEP=0;
int OUT_CONTROL_FLAG=0;
/*
	EEPROM.C
*/
int EEPROM_WEITE_FLAG=0;
int EEPROM_ERASE_FLAG=0;

unsigned int EEPROM_DATA[64];

void Variable_Init(void)
{
	Current_queue.capcity=11;
	Current_queue.head=0;
	Current_queue.back=0;
	Current_queue.size=0;
	for(int i=0;i<Current_queue.capcity;i++){
		Current_queue.queue[i]=0;
	}
}


