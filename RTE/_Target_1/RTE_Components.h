
/*
 * Auto generated Run-Time-Environment Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'HVC_BDC' 
 * Target:  'Target 1' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H


/*
 * Define the Device Header File: 
 */
#define CMSIS_device_header "HVC5221D_B1.h"

/*  Micronas::Device:API:1.0.0 */
#define CMSIS_device_API_header "HVC5221D_B1_API.h"


#endif /* RTE_COMPONENTS_H */
