/**
**************************************************************************
** Copyright notice
** � 2003 Micronas GmbH. All rights reserved
**
** This software and related documentation (the "Software") are intellectual
** property owned by Micronas and are copyright of Micronas, unless specifically
** noted otherwise. Any use of the Software is permitted only pursuant to the
** terms of the license agreement, if any, which accompanies, is included with
** or applicable to the Software ("License Agreement") or upon express written
** consent of Micronas. Any copying, reproduction or redistribution of the
** Software in whole or in part by any means not in accordance with the License
** Agreement or as agreed in writing by Micronas is expressly prohibited.
**
** THE SOFTWARE IS WARRANTED, IF AT ALL, ONLY ACCORDING TO THE TERMS OF THE
** LICENSE AGREEMENT. EXCEPT AS WARRANTED IN THE LICENSE AGREEMENT THE SOFTWARE
** IS DELIVERED "AS IS" AND MICRONAS HEREBY DISCLAIMS ALL WARRANTIES AND CONDITIONS
** WITH REGARD TO THE SOFTWARE, INCLUDING ALL IMPLIED WARRANTIES AND CONDITIONS OF
** MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, QUIT ENJOYMENT, TITLE AND
** NON-INFRINGEMENT OF ANY THIRD PARTY INTELLECTUAL PROPERTY OR OTHER RIGHTS WHICH
** MAY RESULT FROM THE USE OR THE INABILITY TO USE THE SOFTWARE. IN NO EVENT SHALL
** MICRONAS BE LIABLE FOR INDIRECT, INCIDENTAL, CONSEQUENTIAL, PUNITIVE, SPECIAL OR
** OTHER DAMAGES WHATSOEVER INCLUDING WITHOUT LIMITATION, DAMAGES FOR LOSS OF
** BUSINESS PROFITS, BUSINESS INTERRUPTION, LOSS OF BUSINESS INFORMATION, AND THE
** LIKE, ARISING OUT OF OR RELATING TO THE USE OF OR THE INABILITY TO USE THE
** SOFTWARE, EVEN IF MICRONAS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES,
** EXCEPT PERSONAL INJURY OR DEATH RESULTING FROM MICRONAS' NEGLIGENCE
**
** ----------------------------------------------------------------------------
**
** \file   startup_HVC5221D_B1.c
**
** \brief  The C startup file.
**
** \page 	startup_page Startup File
**
** \brief  The C startup file contains the vector table and the default handlers.
**				 In addition the size of the stack can be configured in this file.
**
** \par Stack Size
**			The STACK section is placed directly after the .data section. It is growing downwards.
**			\n The \ref stack_size must be adjusted to the application needs.
**			
**
** -----------------------------------------------------------------------
** \par  Edition History
** - V0.01, August 31, 2016
**       - first version
** - V0.02, April 18, 2017
**       - renamed __initial_sp to __stack
**				 avoids mix-up with __initial_sp from startup_HVC4223F_C1.s file
** - V0.03, March 15, 2018
**       - adapted for HVC4220F-A1 (FLASH1_IRQHandler added)
** - V0.04, December 5, 2018
**       - adapted for HVC4263R-A1 (EPWM2, CAPCOM2, SPI, HSBVDD, PWMIO, Flash1 removed; Flash replaced by CROM)
** - V0.05, August 11, 2021
**       - adapted for HVC5221D-A1 (updated interrupts, added debug interface enable & SDA Reset disable words)
**       - adapted for compiler version 6
** - V0.06, March 14, 2022
**       - adapted definition of initial stack pointer
** - V0.07, May 31, 2022
**       - copied for HVC5221D-B1 from HVC5221D-A1
**
**************************************************************************
*/

/*****************************************************************************/
/** @addtogroup stack_size stack size
 *  @{ */
/*****************************************************************************/
/** The stack size in bytes. */
#define __stack_size 0x100

#define DEBUG_IF_ENABLED  					// comment to disable the debug interface
#define SDA_RESET_DISABLED					// comment to enable the reset input at SDA pin






/** @}		end of group stack_size */


// Initial stack pointer definition
#if defined(__CC_ARM)                 /* ARM compiler V5 */
__attribute__((aligned(8))) unsigned char __stack[__stack_size] __attribute__((section("STACK"), zero_init));
#elif defined (__ARMCC_VERSION) && (__ARMCC_VERSION >= 6100100)  /* ARM compiler V6.10 and later */
__attribute__((aligned(8))) unsigned char __stack[__stack_size] __attribute__((section(".bss.STACK")) );
#else
  #warning Not supported compiler type
#endif

#ifdef DEBUG_IF_ENABLED
	#define dbg_if_ena_word 0xFFFFFFFF
#else
	#define dbg_if_ena_word 0x00000000
#endif

#ifdef SDA_RESET_DISABLED
	#define sda_res_disa_word 0x1234ABCD
#else
	#define sda_res_disa_word 0x00000000
#endif


// prototypes for exception & interrupt handlers
void Reset_Handler(void);								//Reset handler
void NMI_Handler(void);									//NMI exception handler
void HardFault_Handler(void);						//HardFault exception handler
void MemManage_Handler(void);						//MemoryManage exception handler
void BusFault_Handler(void);						//BusFault exception handler
void UsageFault_Handler(void);					//UsageFault exception handler
void SVC_Handler(void);									//SupervisorCall exception handler
void DebugMon_Handler(void);						//DebugMonitor exception handler
void PendSV_Handler(void);							//PendSV exception handler
void SysTick_Handler(void);							//SysTick exception handler

void LGPIO_IRQHandler(void);						//LGPIO interrupt handler
void TIM0_IRQHandler(void);							//TIM0 interrupt handler
void TIM1_IRQHandler(void);							//TIM1 interrupt handler
void LINUART_IRQHandler(void);					//LINUART interrupt handler
void EPWM0_IRQHandler(void);						//EPWM0 interrupt handler
void EPWM1_IRQHandler(void);						//EPWM1 interrupt handler
void EPWM2_IRQHandler(void);						//EPWM2 interrupt handler
void EPWMOC_IRQHandler(void);						//EPWMOC interrupt handler
void CAPCOM0_IRQHandler(void);					//CAPCOM0 interrupt handler
void CAPCOM1_IRQHandler(void);					//CAPCOM1 interrupt handler
void CAPCOM2_IRQHandler(void);					//CAPCOM2 interrupt handler
void ADC_IRQHandler(void);							//ADC interrupt handler
void BEMFC_IRQHandler(void);						//BEMFC interrupt handler
void SPI_IRQHandler(void);							//SPI interrupt handler
void LIN_IRQHandler(void);							//LIN interrupt handler
void FLASH_IRQHandler(void);						//FLASH interrupt handler
void BVDDUV_IRQHandler(void);						//BVDDUV interrupt handler
void BVDDOV_IRQHandler(void);						//BVDDOV interrupt handler
void CPOFF_IRQHandler(void);						//CPOFF interrupt handler

extern int main(void);									//main program
extern int __scatterload(void);					//scatterloader

///////////////////////////////////////////////////////////////////////////////////
// --------------------   EXCEPTION & INTERRUPT VECTOR TABLE   --------------------
///////////////////////////////////////////////////////////////////////////////////
void (* const __Vectors[])(void) __attribute__((section("RESET"))) =
{
	// -----------------------------   ARM EXCEPTIONS   -----------------------------
	(void (*)())&__stack[__stack_size],	// Top of Stack
	Reset_Handler,						// Reset						- enabled
	NMI_Handler,							// NMI							- enabled (not connected)
	HardFault_Handler,				// Hard Fault				- enabled 
	MemManage_Handler,				// MPU Fault				- disabled
	BusFault_Handler,					// Bus Fault				- disabled
	UsageFault_Handler,				// Usage Fault			- disabled
	0,												// 									- reserved 
	(void (*)())dbg_if_ena_word,					// 									-	DEBUG IF ENABLE WORD 
	(void (*)())sda_res_disa_word,				// 									- SDA RESET DISABLE WORD 
	0,												// 									- reserved
	SVC_Handler,							// SVCall						- enabled (SW generated)
	DebugMon_Handler,					// Debug Monitor		- disabled
	0,												// 									-	reserved
	PendSV_Handler,						// PendSV						- enabled (SW generated)
	SysTick_Handler,					// SysTick					- enabled (SysTick Timer disabled)
	// -------------------------------   INTERRUPTS   -------------------------------
	LGPIO_IRQHandler,         // LGPIO IR 				- disabled
	TIM0_IRQHandler,          // TIM0 IR 				- disabled
	TIM1_IRQHandler,          // TIM1 IR 				- disabled
	LINUART_IRQHandler,       // LINUART IR 			- disabled
	EPWM0_IRQHandler,         // EPWM0 IR 				- disabled
	EPWM1_IRQHandler,         // EPWM1 IR 				- disabled
	EPWM2_IRQHandler,         // EPWM2 IR 				- disabled
	EPWMOC_IRQHandler,        // EPWMOC IR 				- disabled
	CAPCOM0_IRQHandler,       // CAPCOM0 IR 			- disabled
	CAPCOM1_IRQHandler,       // CAPCOM1 IR 			- disabled
	CAPCOM2_IRQHandler,       // CAPCOM2 IR 			- disabled
	ADC_IRQHandler,           // ADC IR 				- disabled
	BEMFC_IRQHandler,         // BEMFC IR 				- disabled
	SPI_IRQHandler,           // SPI IR 				- disabled
	LIN_IRQHandler,           // LIN IR 				- disabled
	FLASH_IRQHandler,         // FLASH IR 				- disabled
	BVDDUV_IRQHandler,        // BVDD_UV IR				- disabled
	BVDDOV_IRQHandler,        // BVDD_OV IR				- disabled
	CPOFF_IRQHandler,         // CPOFF IR				- disabled
};


///////////////////////////////////////////////////////////////////////////////////
// ------------------------------   RESET HANDLER   -------------------------------
///////////////////////////////////////////////////////////////////////////////////
__attribute((noreturn)) void Reset_Handler(void)
{
	__scatterload();		// initialize variables and executes __rt_entry()
	while(1) {};
}


// This custom definition of __rt_entry() saves code as several subfunctions 
// (__user_setup_stackheap, __rt_lib_init, __rt_lib_shutdown, __sys_exit) 
// will not be included...
void __rt_entry(void)
{
	main();							// jump to application main routine
}

// NOTE: If __rt_entry() is not defined, a custom version of 
// __user_setup_stackheap() is required (as the default version will get stuck in 
// a breakpoint instruction)!
//void __user_setup_stackheap(void)
//{
//}


///////////////////////////////////////////////////////////////////////////////////
// ----------------------   EXCEPTION & INTERRUPT HANDLERS   ----------------------
///////////////////////////////////////////////////////////////////////////////////
// dummy handlers for all exceptions and interrupts, defined as "weak" to be able 
// to overwrite them in the application
__attribute__((weak)) void NMI_Handler(void)
{
	while(1) {};
}

__attribute__((weak)) void HardFault_Handler(void)
{
	while(1) {};
}

__attribute__((weak)) void MemManage_Handler(void)
{
	while(1) {};
}

__attribute__((weak)) void BusFault_Handler(void)
{
	while(1) {};
}

__attribute__((weak)) void UsageFault_Handler(void)
{
	while(1) {};
}

__attribute__((weak)) void SVC_Handler(void)
{
	while(1) {};
}

__attribute__((weak)) void DebugMon_Handler(void)
{
	while(1) {};
}

__attribute__((weak)) void PendSV_Handler(void)
{
	while(1) {};
}

__attribute__((weak)) void SysTick_Handler(void)
{
	while(1) {};
}

__attribute__((weak)) void LGPIO_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void TIM0_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void TIM1_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void LINUART_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void EPWM0_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void EPWM1_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void EPWM2_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void EPWMOC_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void CAPCOM0_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void CAPCOM1_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void CAPCOM2_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void ADC_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void BEMFC_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void SPI_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void LIN_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void FLASH_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void BVDDUV_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void BVDDOV_IRQHandler(void)
{
	while(1) {};
}

__attribute__((weak)) void CPOFF_IRQHandler(void)
{
	while(1) {};
}


