#include "EEPROM.h"
#include "string.h"
#include "Variable.h"

void EEPROM_DATA_READ(void)
{
	memcpy(EEPROM_DATA,(void *)FLASH->EEPROM0,64);
}

/*
	Flash读写擦除操作流程：
	1、首先等待FLASH准备完毕
	2、解锁需要读写Memory的AB两个寄存器
	3、必须先擦除再写入
*/


unsigned int EEPROM_WRITE(void)
{
	unsigned int num;
	while(FLASH->SR_b.RDY==0);
	FLASH->EULA = ULA_EEPROM; 						// write unlock key A
	FLASH->EULB = ~ULA_EEPROM; 						// write unlock key B
	ERASE_EEPROM_SECTOR((unsigned int)FLASH->EEPROM0,CTRL_E_EEPROM,0);
	
	num=WRITE_EEPROM_WORD((unsigned int)&FLASH->EEPROM0[0],LOCKED_ROTOR_ERROR_COUNT,CTRL_W_EEPROM,0);
	FLASH->EULA = 0; 								// Lock customer EEPROM 
	return num;
}

unsigned int READ_EEPROM_WORD(unsigned int addr, unsigned int *data)
{
	if ((addr>=0x2A0000) && (addr<=0x2A01ff))					// check if address is inside EEPROM
	{
		if (FLASH->EULA == 0x000000ff)									// check if EEPROM is locked
		{
			return(4);																		// finish with error response 4 if EEPROM is locked
		}
	}
	else
	{
		return(1);																			// finish with error response 1 if addr is outside EEPROM
	}
	if (FLASH->SR_b.RDY == 0)													// check if flash is ready
	{
		return(8);																			// finish with error response 8 if flash is not ready
	}
	else																							// do page programming
	{
		*data=addr;
		return 0;
	}
}

/*
addr:写入地址是否正确，data:写入数据，ctrl:控制模式，cmd:检查标志
*/
unsigned int WRITE_EEPROM_WORD(unsigned int addr, unsigned int data, unsigned int ctrl, unsigned int cmd)
{
	if ((addr>=0x2A0000) && (addr<=0x2A01ff))					// check if address is inside EEPROM
	{
		if (ctrl != CTRL_W_EEPROM)											// check control word
		{
			return(2);																		// finish with error response 2 if ctrl is wrong
		}
		if (FLASH->EULA == 0x000000ff)									// check if EEPROM is locked
		{
			return(4);																		// finish with error response 4 if EEPROM is locked
		}
	}
	else
	{
		return(1);																			// finish with error response 1 if addr is outside EEPROM
	}

	if (FLASH->SR_b.RDY == 0)													// check if flash is ready
	{
		return(8);																			// finish with error response 8 if flash is not ready
	}
	else																							// do page programming
	{
		unsigned int __IO blankcheck;													// read data for blank-checking (defined as __IO to avoid removal of read-access due to compiler optimization)
		unsigned int *memptr;															// pointer to target address
		memptr = (unsigned int *) (addr & 0xfffffffc);									// make addr 32bit word aligned

		__disable_irq();																// disable interrupts during critical phase
		blankcheck = (unsigned int) *memptr;											// read target address for blank-checking
		
		if (FLASH->SR_b.BLNKE == 0)														// if read EEPROM word is not erased
		{
			__enable_irq();																// re-enable interrupts
			return (16);																	// finish with error response 16 if addr is not blank
		}

		*memptr = data;																	// write data to addr

		FLASH->CR_b.PRG_STRT = 1;												// start programming
		__NOP();
		__enable_irq();																	// re-enable interrupts
		
		if (cmd == 1)																		// check if exit without waiting for RDY is requested
			return (256);																	// response 256 when exiting without waiting for RDY

		while (FLASH->SR_b.RDY == 0);										// wait for FLASH READY

		return (0);																			// finished programming, response 0
	}
}

//-----------------------------------------------------------------------------
// Subroutine for erasing a sector of the EEPROM
//-----------------------------------------------------------------------------
unsigned int ERASE_EEPROM_SECTOR(unsigned int addr, unsigned int ctrl, unsigned int cmd)
{
	if ((addr>=0x2A0000) && (addr<=0x2A01ff))					// check if address is inside EEPROM
	{
		if (ctrl != CTRL_E_EEPROM)											// check control word
		{
			return(2);																		// finish with error response 2 if ctrl is wrong
		}
		else if (FLASH->EULA == 0x000000ff)							// check if EEPROM is locked
		{
			return(4);																		// finish with error response 4 if EEPROM is locked
		}
	}
	else
	{
		return(1);																			// finish with error response 1 if addr is outside EEPROM
	}

	if (FLASH->SR_b.RDY == 0)													// check if flash is ready
	{
		return(8);																			// finish with error response 8 if flash is not ready
	}
	else																							// do sector erase
	{
		unsigned int *memptr;														// pointer to flash address
		memptr = (unsigned int *) (addr & 0xfffffffc);	// make addr 32bit word aligned
		
		__disable_irq();																// disable interrupts during critical phase
		FLASH->CR_b.CHIP = 0;														// disable full erase
		*memptr = 0;																		// select sector
		FLASH->CR_b.ERA_STRT = 1;												// start erase
		__NOP();
		__enable_irq();																	// re-enable interrupts
		
		if (cmd == 1)																		// check if exit without waiting for RDY is requested
			return (256);																	// response 256 when exiting without waiting for RDY

		while (FLASH->SR_b.RDY == 0);										// wait for FLASH READY
		
		return (0);																			// finished erasing, response 0
	}
}

