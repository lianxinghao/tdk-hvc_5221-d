#include "EPWM.h"
#include "TIM.h"
#include "ADC.h"
#include "Variable.h"
#include "EEPROM.h"
void EPWM_Init(void){
	/****************CR0****************/
	EPWM->CR0_b.EN=1;	//开关
	EPWM->CR0_b.PAM=0;	//PWM校准模式0：中心校准 1：边缘校准
	EPWM->CR0_b.SR=0;	//0：正常模式 1：快速模式
	EPWM->CR0_b.OCSDM=0;	//0；过流关闭所有半桥 1：过流仅仅关闭过流半桥
	EPWM->CR0_b.CCPT=13;	//0-15 
	/*
	交叉电流保护时间（ns）
	0 bypassed 约等于50
	1 200
	2 400
	3 600
	4 800
	5 1000
	6 1200
	7 1400
	8 1600
	9 1800
	10 2000
	11 2200
	12 2400
	13 2600
	14 2800
	15 3000
	*/
	EPWM->CR0_b.CKSEL=0;	
	/*
	EPWM CLOCK FSYS 总频率
	0:20MHZ 
	1:10MHZ 
	2:5MHZ 
	3:2.5MHZ 
	4:1.25MHZ 
	5:0.625MHZ 
	6:0.3125MHZ 
	7:0.15625MHZ
	*/
	EPWM->CR0_b.OCFT=0;
	/*
	过流Filter Time us
	set min max
	0 4 4.4
	1 8 8.8
	2 12 13.2
	3 16 17.6
	4 20 22
	5 24 26.4
	6 28 30.8
	7 32 35.2
	*/
	EPWM->CR0_b.BEMFM=0;
	EPWM->CR0_b.BCM=0;	
	/*
	0:全为单边桥模式 
	1:0,1成对 	2,3为单边桥 
	2:0,1单边桥 2,3成对 
	3:0,1成对 	2,3成对
	*/
	EPWM->CR0_b.BLKTM=0;
	EPWM->CR0_b.HSOCDIS=0; //高边过流开关使能
	EPWM->CR0_b.GM=0;		//门驱动模式使能，外部MOS
	
	/****************CR1****************/
	EPWM->CR1_b.LSOM0=0;
	EPWM->CR1_b.HSOM0=0;
	EPWM->CR1_b.LSOM1=0;
	EPWM->CR1_b.HSOM1=0;
	EPWM->CR1_b.LSOM2=0;
	EPWM->CR1_b.HSOM2=0;
	EPWM->CR1_b.LSOM3=0;
	EPWM->CR1_b.HSOM3=0;
	//0：关闭mos 1：开启mos 2：接入反向EPWM输出 3：接入EPWM输出
	
	EPWM->CR1_b.INSEL0=0;
	EPWM->CR1_b.INSEL1=1;
	EPWM->CR1_b.INSEL2=2;
	EPWM->CR1_b.INSEL3=3;
	/*
	全桥控制源选择
	0 PWM0
	1 PWM1
	2 PWM2
	3 TIMER1
	*/
	
	EPWM->CR1_b.BEMFSEL=0;
	EPWM->CR1_b.PWM2M=0;
	EPWM->CR1_b.CLM0=0;
	EPWM->CR1_b.CLM1=0;
	EPWM->CR1_b.CLM2=0;
	EPWM->CR1_b.CLMIM=0;
	//电流限制模式选择0：关闭 1：开启
	
	/****************CR2****************/
	
	EPWM->CR2_b.PRBE0=1;
	EPWM->CR2_b.PRBE1=1;
	EPWM->CR2_b.PRBE2=1;
	//寄存器缓存
	EPWM->CR2_b.PRUM0=1;
	EPWM->CR2_b.PRUM1=1;
	EPWM->CR2_b.PRUM2=1;
	
	EPWM->CR2_b.PUR0=0;
	EPWM->CR2_b.PUR1=0;
	EPWM->CR2_b.PUR2=0;
	
	EPWM->CR2_b.CLBE=0;
	EPWM->CR2_b.CLUEOP=0;
	EPWM->CR2_b.CLUTRG=0;
	
	EPWM->CR2_b.CR1BE=0;
	EPWM->CR2_b.CR1UEOP=0;
	EPWM->CR2_b.CR1UTRG=0;
	
	/****************TCR****************/
	
	/****************PERx****************/
	
	EPWM->PER0_b.PER=999;
	EPWM->PER1_b.PER=999;
	EPWM->PER2_b.PER=999;
	//PWM周期设置
	
	/****************COMPx****************/
	EPWM->COMP0_b.COMP=0;
	EPWM->COMP1_b.COMP=0;
	EPWM->COMP2_b.COMP=999;
	//占空比初始化=COMP/PER+1
	/****************CAPx****************/
	
	/****************TRGx****************/
	
	
	/****************CLDAC****************/
	EPWM->CLDAC_b.DATA0=128;
	EPWM->CLDAC_b.DATA1=128;
	/****************IEN****************/
	/*IEN内部有四种中断类型可选择，每种中断类型都对应一个中断使能*/
	NVIC_SetPriority(EPWM0_IRQn, EPWM0_IRQ_PRIO);
	EPWM->IEN_b.EOP0=1;//PWM中断发生在周期结束时
	
	NVIC_SetPriority(EPWM1_IRQn, EPWM1_IRQ_PRIO);
	EPWM->IEN_b.EOP1=1;
	
	NVIC_SetPriority(EPWM2_IRQn, EPWM2_IRQ_PRIO);
	EPWM->IEN_b.EOP2=1;
	
	NVIC_SetPriority(EPWMOC_IRQn, EPWMOC_IRQ_PRIO);
	EPWM->IEN_b.OC=1;  //过电流中断    还没开起来不知道原因
	
	/****************EPENDING****************/
	EPWM->EIPND_b.EOP0=1;//使能周期结束中断
	NVIC_EnableIRQ(EPWM0_IRQn);;		/* enable in NVIC */
	
	EPWM->EIPND_b.EOP1=1;
	NVIC_EnableIRQ(EPWM1_IRQn);;		/* enable in NVIC */
	
	EPWM->EIPND_b.EOP2=1;
	NVIC_EnableIRQ(EPWM2_IRQn);;		/* enable in NVIC */
	
	EPWM->EIPND_b.OC=1;	//使能过电流中断
	NVIC_EnableIRQ(EPWMOC_IRQn);			/* enable in NVIC */


}



/*
八步 开阀 逆时针   6个八拍为一圈，48个Beat为一圈
*/

void Motor_Run(void)
{
	/* 两次堵转说明初始化完成 */
	if(LOCKED_ROTOR_ERROR_COUNT<2)
	{
		if(LOCKED_ROTOR_ERROR==1)
		{
			Dir_Flag=(Dir_Flag+1)%2;
			LOCKED_ROTOR_ERROR=0;
			Set_Mos_State(0,0,0,0,0,0,0,0);	
		}
		Eight_Beat_Open_EXV(30000,Time);//就当设置步数上限，避免堵转未判断出时不停
		Eight_Beat_Close_EXV(30000,Time);//1：总转动步数 2：每步转动时间0.01ms
		
		if(Save_Beat-Last_Save_Beat>=96&&Time!=Min_time)
		{
			Time=Time-10;
			Last_Save_Beat=Save_Beat;
		}
	}else//初始化结束根据接受指令：开关，开关步数（未设置的话就默认一直开或关），开关速度（就默认）
	{
		if(LOCKED_ROTOR_ERROR==1)
		{
			Dir_Flag=(Dir_Flag+1)%2;
			LOCKED_ROTOR_ERROR=0;
			Set_Mos_State(0,0,0,0,0,0,0,0);	
		}
		if(OPEN_EXV_FLAG==1)
		{
			if(EXV_STEP>INIT_ALL_STEP)EXV_STEP=INIT_ALL_STEP;
			Eight_Beat_Open_EXV(EXV_STEP,Time);
			if(Save_Beat-Last_Save_Beat>=96&&Time!=Min_time)
			{
				Time=Time-10;
				Last_Save_Beat=Save_Beat;
			}
		}else
		if(CLOSE_EXV_FLAG==1)
		{
			if(EXV_STEP>INIT_ALL_STEP)EXV_STEP=INIT_ALL_STEP;
			Eight_Beat_Close_EXV(EXV_STEP,Time);
			if(Save_Beat-Last_Save_Beat>=96&&Time!=Min_time)
			{
				Time=Time-10;
				Last_Save_Beat=Save_Beat;
			}
		}
	}
}
void Eight_Beat_Open_EXV(int Rotate,int Step_Time)
{
	if(OPEN_EXV_FLAG==1)Dir_Flag=1;
	if(Dir_Flag==0)return ;
//	/* 过流故障恢复处理 */
//	if(OVER_CURRENT_ERROR==1&&TIM0_Handler_Counter-ERROR_HAPPENED_TIME>=ERROR_RECOVER_TIME&&(ERROR_RECOVER<ERROR_RECOVER_COUNT)){
//		/* 当故障发生时间到了2s开始尝试恢复 */
//		OVER_CURRENT_ERROR=0;
//		ERROR_RECOVER++;
//	}
//	/* 堵转后反转, */
//	if(ADC_Voltage.Motor_Current==0x7fff){
//		LOCKED_ROTOR_ERROR=0;
//	}
	if(OPEN_EXV_FLAG==1&&Rotate==0)Rotate=INIT_ALL_STEP;
	if(Save_Beat<=Rotate&&OVER_CURRENT_ERROR==0&&OPEN_CIRCLE_ERROR==0){/* 函数执行的条件：步数没跑完，未发生故障 */
		if(TIM0_Handler_Counter-Change_Eight_Beat_Counter>=Step_Time){
			if(((Motor_State_Open_Real+1)%8)==MOTOR_DIR){				
				switch(MOTOR_DIR){
					case 0://A+启动
						Mos_State[0]=1;
						Mos_State[1]=0;
						Mos_State[2]=0;
						Mos_State[3]=1;
						Mos_State[4]=0;
						Mos_State[5]=0;
						Mos_State[6]=0;
						Mos_State[7]=0;
					break;
					case 1://A+B+
						Mos_State[0]=1;
						Mos_State[1]=0;
						Mos_State[2]=0;
						Mos_State[3]=1;
						Mos_State[4]=1;
						Mos_State[5]=0;
						Mos_State[6]=0;
						Mos_State[7]=1;
					break;
					case 2://B+
						Mos_State[0]=0;
						Mos_State[1]=0;
						Mos_State[2]=0;
						Mos_State[3]=0;
						Mos_State[4]=1;
						Mos_State[5]=0;
						Mos_State[6]=0;
						Mos_State[7]=1;		
					break;
					case 3://B+A-
						Mos_State[0]=0;
						Mos_State[1]=1;
						Mos_State[2]=1;
						Mos_State[3]=0;
						Mos_State[4]=1;
						Mos_State[5]=0;
						Mos_State[6]=0;
						Mos_State[7]=1;
					break;
					case 4://A-
						Mos_State[0]=0;
						Mos_State[1]=1;
						Mos_State[2]=1;
						Mos_State[3]=0;
						Mos_State[4]=0;
						Mos_State[5]=0;
						Mos_State[6]=0;
						Mos_State[7]=0;
					break;
					case 5://A-B-
						Mos_State[0]=0;
						Mos_State[1]=1;
						Mos_State[2]=1;
						Mos_State[3]=0;
						Mos_State[4]=0;
						Mos_State[5]=1;
						Mos_State[6]=1;
						Mos_State[7]=0;
					break;
					case 6://B-
						Mos_State[0]=0;
						Mos_State[1]=0;
						Mos_State[2]=0;
						Mos_State[3]=0;
						Mos_State[4]=0;
						Mos_State[5]=1;
						Mos_State[6]=1;
						Mos_State[7]=0;
					break;
					case 7://B-A+
						Mos_State[0]=1;
						Mos_State[1]=0;
						Mos_State[2]=0;
						Mos_State[3]=1;
						Mos_State[4]=0;
						Mos_State[5]=1;
						Mos_State[6]=1;
						Mos_State[7]=0;
					break;
				}
				Last_MOTOR_DIR=MOTOR_DIR;
				MOTOR_DIR++;
				Save_Beat++;
				if(MOTOR_DIR==8){
					MOTOR_DIR=0;
				}
				Change_Eight_Beat_Counter=TIM0_Handler_Counter;
			}
		}
		Set_Mos_State(Mos_State[0],Mos_State[1],Mos_State[2],Mos_State[3],Mos_State[4],Mos_State[5],Mos_State[6],Mos_State[7]);
	}else{
		if(Save_Beat>=Rotate){
			Rotate_Finish=1;
			//外部动作完成清空标志
			if(OPEN_EXV_FLAG==1){
				Value_Recover();
				OPEN_EXV_FLAG=0;
			}
		}
		Mos_State[0]=0;
		Mos_State[1]=0;
		Mos_State[2]=0;
		Mos_State[3]=0;
		Mos_State[4]=0;
		Mos_State[5]=0;
		Mos_State[6]=0;
		Mos_State[7]=0;
		Set_Mos_State(Mos_State[0],Mos_State[1],Mos_State[2],Mos_State[3],Mos_State[4],Mos_State[5],Mos_State[6],Mos_State[7]);
	}
}


void Eight_Beat_Close_EXV(int Rotate,int Step_Time)
{
	if(CLOSE_EXV_FLAG==1)Dir_Flag=0;
	if(Dir_Flag==1)return ;
//	/* 过流故障恢复处理 */
//	if(OVER_CURRENT_ERROR==1&&TIM0_Handler_Counter-ERROR_HAPPENED_TIME>=ERROR_RECOVER_TIME&&(ERROR_RECOVER<ERROR_RECOVER_COUNT)){
//		/* 当故障发生时间到了2s开始尝试恢复 */
//		OVER_CURRENT_ERROR=0;
//		ERROR_RECOVER++;
//	}
//	/* 堵转后反转, */
//	if(ADC_Voltage.Motor_Current==0x7fff){
//		LOCKED_ROTOR_ERROR=0;
//	}
	if(CLOSE_EXV_FLAG==1&&Rotate==0)Rotate=INIT_ALL_STEP;
	if(Save_Beat<=Rotate&&OVER_CURRENT_ERROR==0&&OPEN_CIRCLE_ERROR==0){/* 函数执行的条件：步数没跑完，未发生故障 */
		if(TIM0_Handler_Counter-Change_Eight_Beat_Counter>=Step_Time){//相当于10MS的延迟
			if(Motor_State_Close_Real==((MOTOR_DIR+1)%8)){
				
				switch(MOTOR_DIR){
					case 0://A+启动
						Mos_State[0]=1;
						Mos_State[1]=0;
						Mos_State[2]=0;
						Mos_State[3]=1;
						Mos_State[4]=0;
						Mos_State[5]=0;
						Mos_State[6]=0;
						Mos_State[7]=0;
					break;
					case 7://A+B-
						Mos_State[0]=1;
						Mos_State[1]=0;
						Mos_State[2]=0;
						Mos_State[3]=1;
						Mos_State[4]=0;
						Mos_State[5]=1;
						Mos_State[6]=1;
						Mos_State[7]=0;
					break;
					case 6://B-
						Mos_State[0]=0;
						Mos_State[1]=0;
						Mos_State[2]=0;
						Mos_State[3]=0;
						Mos_State[4]=0;
						Mos_State[5]=1;
						Mos_State[6]=1;
						Mos_State[7]=0;		
					break;
					case 5://B-A-
						Mos_State[0]=0;
						Mos_State[1]=1;
						Mos_State[2]=1;
						Mos_State[3]=0;
						Mos_State[4]=0;
						Mos_State[5]=1;
						Mos_State[6]=1;
						Mos_State[7]=0;
					break;
					case 4://A-
						Mos_State[0]=0;
						Mos_State[1]=1;
						Mos_State[2]=1;
						Mos_State[3]=0;
						Mos_State[4]=0;
						Mos_State[5]=0;
						Mos_State[6]=0;
						Mos_State[7]=0;
					break;
					case 3://A-B+
						Mos_State[0]=0;
						Mos_State[1]=1;
						Mos_State[2]=1;
						Mos_State[3]=0;
						Mos_State[4]=1;
						Mos_State[5]=0;
						Mos_State[6]=0;
						Mos_State[7]=1;
					break;
					case 2://B+
						Mos_State[0]=0;
						Mos_State[1]=0;
						Mos_State[2]=0;
						Mos_State[3]=0;
						Mos_State[4]=1;
						Mos_State[5]=0;
						Mos_State[6]=0;
						Mos_State[7]=1;
					break;
					case 1://B+A+
						Mos_State[0]=1;
						Mos_State[1]=0;
						Mos_State[2]=0;
						Mos_State[3]=1;
						Mos_State[4]=1;
						Mos_State[5]=0;
						Mos_State[6]=0;
						Mos_State[7]=1;
					break;
				}
				Last_MOTOR_DIR=MOTOR_DIR;
				MOTOR_DIR--;
				Save_Beat++;
				if(MOTOR_DIR==-1){
					MOTOR_DIR=7;
				}
				Change_Eight_Beat_Counter=TIM0_Handler_Counter;
			}
		}
		Set_Mos_State(Mos_State[0],Mos_State[1],Mos_State[2],Mos_State[3],Mos_State[4],Mos_State[5],Mos_State[6],Mos_State[7]);
	}else{
		if(Save_Beat>=Rotate){
			Rotate_Finish=1;
			//外部动作完成清空标志
			if(CLOSE_EXV_FLAG==1)
			{
				Value_Recover();
				CLOSE_EXV_FLAG=0;
			}
		}
		Mos_State[0]=0;
		Mos_State[1]=0;
		Mos_State[2]=0;
		Mos_State[3]=0;
		Mos_State[4]=0;
		Mos_State[5]=0;
		Mos_State[6]=0;
		Mos_State[7]=0;
		Set_Mos_State(Mos_State[0],Mos_State[1],Mos_State[2],Mos_State[3],Mos_State[4],Mos_State[5],Mos_State[6],Mos_State[7]);
	}
}


void Set_Mos_State(int H0,int L0,int H1,int L1,int H2,int L2,int H3,int L3)
{
	EPWM->CR1_b.HSOM0=H0;
	EPWM->CR1_b.LSOM0=L0;
	EPWM->CR1_b.HSOM1=H1;
	EPWM->CR1_b.LSOM1=L1;
	EPWM->CR1_b.HSOM2=H2;
	EPWM->CR1_b.LSOM2=L2;
	EPWM->CR1_b.HSOM3=H3;
	EPWM->CR1_b.LSOM3=L3;
}
	



int EPWM0_Handler_Counter=0;
void EPWM0_IRQHandler(void)
{
	u32 pending_u32;
	pending_u32 = EPWM->EIPND&0x00FF;	/* read enabled interrupts pending flag EPWM1  */
	EPWM->EIPND = pending_u32;				/* clear enabled interrupts pending flags EPWM1 */
	if(pending_u32&0x01)						/* if EOP1 flag is set */
	{	
		EPWM0_Handler_Counter++;
	}
}


int EPWM1_Handler_Counter=0;
void EPWM1_IRQHandler(void)
{
	u32 pending_u32;
	pending_u32 = EPWM->EIPND&0xFF00;	/* read enabled interrupts pending flag EPWM1  */
	EPWM->EIPND = pending_u32;				/* clear enabled interrupts pending flags EPWM1 */
	if(pending_u32&0x0100)						/* if EOP1 flag is set */
	{	
		EPWM1_Handler_Counter++;
	}
}


int EPWM2_Handler_Counter=0;
void EPWM2_IRQHandler(void)
{
	u32 pending_u32;
	pending_u32 = EPWM->EIPND&0xFF0000;	/* read enabled interrupts pending flag EPWM1  */
	EPWM->EIPND = pending_u32;				/* clear enabled interrupts pending flags EPWM1 */
	if(pending_u32&0x010000)						/* if EOP1 flag is set */
	{	
		EPWM2_Handler_Counter++;
	}
}


int EPWMOC_Handler_Counter=0;
void EPWMOC_IRQHandler(void)
{
	u32 pending_u32;
	pending_u32 = EPWM->EIPND&0xFF000000;		/* read enabled interrupts pending flag EPWM0  */
	EPWM->EIPND = pending_u32;				/* clear enabled interrupts pending flags */
	if(pending_u32&0x01000000)				/* check OC0 flag */
	{	
		EPWMOC_Handler_Counter++;
	}	
}


