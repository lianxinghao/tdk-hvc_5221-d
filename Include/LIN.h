
#ifndef __LIN_H
#define __LIN_H

#include "config.h"

void LIN_Init(void);
void Write_TXD_Register(char Data);
char Receive_RXD_Register(void);


#endif /* #ifndef __LIN_H */


