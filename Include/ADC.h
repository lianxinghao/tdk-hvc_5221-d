
#ifndef __ADC_H
#define __ADC_H

#include "config.h"
extern int Motor_State_Open_Real;
extern int Motor_State_Close_Real;
extern int Open_Really_All_Step;
extern int Close_Really_All_Step;
extern struct ADC_Value ADC_Voltage;
struct ADC_Raw_Value{
	int Raw_AVSS;
	int Raw_BVDD;
	int Raw_RS0;
	int Raw_RS0_ref;
	int Raw_MOUT3;
	int Raw_MOUT2;
	int Raw_MOUT1;
	int Raw_MOUT0;
	int Raw_TEMPERATURE;
	int Raw_RS1;
	int Raw_RS1_ref;
};
struct ADC_Value{
	int ADC_Queue_AVSS_Current;
	int ADC_Queue_RS0_Current;
	int ADC_Queue_MOUT3_Voltage;
	int ADC_Queue_MOUT2_Voltage;
	int ADC_Queue_MOUT1_Voltage;
	int ADC_Queue_MOUT0_Voltage;
	int ADC_Queue_TMP_SENSOR_Voltage;
	int ADC_Queue_RS1_Current;
	int ADC_Queue_RS_Current;
	int RS0_Average;
	int RS1_Average;
	int RS_Average;
	double Motor_Current; //计算得到总得电流值mA
	double Motor_Current_Average;	/* 500采样点的平均电流 */
};

struct ADC_Data{
	int MOUT3_Voltage;
	int MOUT2_Voltage;
	int MOUT1_Voltage;
	int MOUT0_Voltage;
	double Temperature;//°C
};

void ADC_Init(void);
void Value_Recover(void);
bool Mos_CLOSE_Judge(void);
void Raw_Data_Get(void);
void Voltage_Process(void);
void Current_Process(void);
void Motor_Position_Judge(void);
void Motor_Step_Count(void);
void ERROR_CHACK(void);
int LOCKED_MOTOR_DETECT_RUNING(void);
#endif /* #ifndef __ADC_H */


