
#ifndef __VARIABLE_H
#define __VARIABLE_H

struct Queue{
	double queue[11];	
	int capcity;
	int size;
	int head;
	int back;
};

#include "config.h"
extern bool OVER_CURRENT_ERROR;	/* 过流故障标志位 */
extern bool OPEN_CIRCLE_ERROR;	/* 开路故障标志位 */
extern bool LOCKED_ROTOR_ERROR;	/*堵转故障标志位 */
extern int ERROR_HAPPENED_TIME;	/* 故障发生时间 */
extern unsigned int LOCKED_ROTOR_ERROR_COUNT;	/*堵转故障发生次数 */

extern bool MOS_STATE_CHANGE;	/* MOS状态更改标志 */


extern int ADC_CURRENT_MID_VALUE;	/* AD电流采集中间值 */
extern int ADC_CURRENT_MAX_VALUE;	/* AD电流采集最大值 */

extern int RS0_CURRENT_POSITION;	/* 电流矢量位置 */
extern int RS0_CURRENT_POSITION_LAST;	/* 上次电流矢量位置 */
extern int RS1_CURRENT_POSITION;	/* 电流矢量位置 */
extern int RS1_CURRENT_POSITION_LAST;	/* 上次电流矢量位置 */
extern int RS0_CURRENT_CALCULATE_FLAG;	/* 电流开始计算标志位 */
extern int RS1_CURRENT_CALCULATE_FLAG;	/* 电流开始计算标志位 */
extern int RS0_AVERAGE_COUNT;
extern int RS1_AVERAGE_COUNT;
extern int RS0_All_CURRENT;
extern int RS1_All_CURRENT;
extern double ADC_RATIO; 	/* 电流比例估算值 */

extern double Motor_Max_Speed_Current;	/* 最大转速电流 */
extern double Motor_Locked_Current;	/* 堵转电流 */
extern double Current_Judge_Gap;	/* 电流偏差 */
extern double Real_Current_Count;
extern int Simple_Number;	/* 采样数量点 */
extern int Real_Count;

extern struct Queue Current_queue;	
extern double Current_Sudden_Change;
extern double MAX_START_CURRENT;
extern double MIN_START_CURRENT;
/*
	EPWM.C
*/
#define Start_Speed 200
extern int Eight_Beat_Time;//单位0.1ms，转动频率，1ms一步
extern int Save_Beat;   //当前步数统计
extern int Change_Eight_Beat_Counter;

extern int Last_MOTOR_DIR;
extern int Dir_Flag;	/* 正反转方向标志 */
extern int MOTOR_DIR;
extern int Mos_State[8];
extern int ERROR_RECOVER;
extern int Rotate_Finish;

extern int Last_Save_Beat;
extern int Time;

extern int CLOSE_EXV_FLAG;
extern int OPEN_EXV_FLAG;
extern int EXV_STEP;
extern int INIT_ALL_STEP;
extern int OUT_CONTROL_FLAG;
/*
	EEPROM.C
*/
extern int EEPROM_WEITE_FLAG;
extern int EEPROM_ERASE_FLAG;
extern unsigned int EEPROM_DATA[64];

void Variable_Init(void);
#endif /* #ifndef __VARIABLE_H */


