
#ifndef __EPWM_H
#define __EPWM_H

#include "config.h"



extern int Mos_State[8];

extern int MOTOR_DIR;
extern int Last_MOTOR_DIR;
extern int Dir_Flag;
extern int Save_Beat;
extern int Change_Eight_Beat_Counter;
extern int Rotate_Finish;
void EPWM_Init(void);
void Eight_Beat_Open_EXV(int Rotate,int Step_Time);//
void Eight_Beat_Close_EXV(int Rotate,int Step_Time);
void Set_Mos_State(int H0,int L0,int H1,int L1,int H2,int L2,int H3,int L3);

extern int EPWM0_Handler_Counter;
extern int EPWM1_Handler_Counter;
extern int EPWM2_Handler_Counter;
extern int EPWMOC_Handler_Counter;

#endif /* #ifndef __EPWM_H */


