/*******************************************************************************
*                                                                             **
*  Legal Notice/Disclaimer                                                    **
*                                                                             **
*  � 2017 Micronas GmbH. All rights reserved                                  **
*  This software and related documentation (the �Software�) are intellectual  **
*  property owned by Micronas and are copyright of Micronas, unless           **
*  specifically noted otherwise.                                              **
*                                                                             **
*  Any use of the Software is permitted only pursuant to the terms of the     **
*  license agreement, if any, which accompanies, is included with or          **
*  applicable to the Software ("License Agreement") or upon express written   **
*  consent of Micronas. Any copying, reproduction or redistribution of the    **
*  Software in whole or in part by any means not in accordance with the       **
*  License Agreement or as agreed in writing by Micronas is expressly         **
*  prohibited.                                                                **
*                                                                             **
*  THE SOFTWARE IS WARRANTED, IF AT ALL, ONLY ACCORDING TO THE TERMS OF THE   **
*  LICENSE AGREEMENT. EXCEPT AS WARRANTED IN THE LICENSE AGREEMENT THE        **
*  SOFTWARE IS DELIVERED �AS IS� AND MICRONAS HEREBY DISCLAIMS ALL WARRANTIES **
*  AND CONDITIONS WITH REGARD TO THE SOFTWARE, INCLUDING ALL IMPLIED          **
*  WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR     **
*  PURPOSE, QUIT ENJOYMENT, TITLE AND NON-INFRINGEMENT OF ANY THIRD PARTY     **
*  INTELLECTUAL PROPERTY OR OTHER RIGHTS WHICH MAY RESULT FROM THE USE OR THE **
*  INABILITY TO USE THE SOFTWARE.                                             **
*                                                                             **
*  IN NO EVENT SHALL MICRONAS BE LIABLE FOR INDIRECT, INCIDENTAL,             **
*  CONSEQUENTIAL, PUNITIVE, SPECIAL OR OTHER DAMAGES WHATSOEVER INCLUDING     **
*  WITHOUT LIMITATION, DAMAGES FOR LOSS OF BUSINESS PROFITS, BUSINESS         **
*  INTERRUPTION, LOSS OF BUSINESS INFORMATION, AND THE LIKE, ARISING OUT OF   **
*  OR RELATING TO THE USE OF OR THE INABILITY TO USE THE SOFTWARE, EVEN IF    **
*  MICRONAS HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES, EXCEPT       **
*  PERSONAL INJURY OR DEATH RESULTING FROM MICRONAS� NEGLIGENCE.              **
*                                                                             **
********************************************************************************
* @file     config.h
*
* @brief    configuration header file
*
* @version  $Revision:  $
* @date     $Date:  $
* @author 	$Author: spirk@MICRONAS.COM $
*******************************************************************************/
#ifndef __CONFIG_H
#define __CONFIG_H

#include "HVC5221D_B1.h"
#include "HVC5221D_B1_API.h"

/** @addtogroup Configuration-Header
  * @{
*/

/*****************************************************************************/
/* �Զ���������������е���Ҫ����
*/
/*****************************************************************************/

#define OVER_CURRENT_LIMIT 2000   /* ��������ֵ��mA�� */
#define OPEN_CIRCLE_LIMIT 30	/* ��·����ֵ��mA�� */
#define OVER_CURRENT_RECOVER_LIMIT 500	/* �����ָ�ֵ��mA�� */
#define OPEN_CIRCLE_RECOVER_LIMIT 50	/* ��·�ָ�ֵ��mA�� */
#define ERROR_RECOVER_TIME 200000		/* ����ָ�ʱ�䣨һ��TIM0 COUNT�� */
#define ERROR_RECOVER_COUNT 10	/* ����ָ����� */
#define STABLE_CURRENT 380		/* ���ת�ٵ��� */
#define MAX_ADD_CURRENT 80		/* ���������� */
#define DEFAULT_LOCKED_CURRENT 500		/* Ĭ�϶�ת���� */
#define REFERENCE_CURRENT 0x3fff		/* ��׼���� */
/*****************************************************************************/
/* Debug
*/
/*****************************************************************************/
#define DEBUG											/* if defined, enables debug ouput on LGPIO */
#define DEBUG_PORT	LGPIO3				/* configure defined LGPIO as output push-pull */
#define DEBUG_PORT2	LGPIO4				/* configure defined LGPIO as output push-pull */
//#define DEBUG_PORT_AS_SWO		(1U)		/* if defined as 1, DEBUG_PORT is configured as SWO alternative output */


/*****************************************************************************/
/** @addtogroup General
 *  @{ */
/*****************************************************************************/
/** enable the ERM module, 0 = disable, 1 = enable */
#define ERM_ENABLE						(0U)
/** enable the digital watchdog module, 0 = disable, 1 = enable */
#define DWDG_ENABLE						(0U)
/** enable the window watchdog module, 0 = disable, 1 = enable */
#define WWDG_ENABLE						(0U)

/** @}		end of group General */


/*****************************************************************************/
/** @addtogroup Interrupt-Priorities
 *  @{ */
/*****************************************************************************/
#define BVDDOV_IRQ_PRIO				(0U)				/* BVDD over-voltage switch off, must be highest priority */
#define BVDDUV_IRQ_PRIO				(0U)				/* BVDD under-voltage switch off, must be highest priority */
#define LINUART_IRQ_PRIO			(0U)
#define LIN_IRQ_PRIO				(0U)
#define EPWM0_IRQ_PRIO    		(1U)				/* EPWM EOP IRQ for duty cycle soft start, priority should be high to not miss a PWM period */
#define EPWM1_IRQ_PRIO    		(1U)				/* EPWM EOP IRQ for duty cycle soft start, priority should be high to not miss a PWM period */
#define EPWM2_IRQ_PRIO    		(1U)				/* EPWM EOP IRQ for duty cycle soft start, priority should be high to not miss a PWM period */
#define TIM0_IRQ_PRIO			(1U)	
#define TIM1_IRQ_PRIO			(1U)
#define ADC_IRQ_PRIO				(2U)				/* ADC end of conversion IRQ */
#define EPWMOC_IRQ_PRIO				(3U)				/* MOUT-OC handling, priority can be low since MOUTs are switched off by hardware */

/** @}		end of group Interrupt-Priorities */


/*****************************************************************************/
/** @addtogroup Module-CMD
 *  @{ */
/*****************************************************************************/


/** @}		end of group Module-CMD */


/*****************************************************************************/
/** @addtogroup Module-MSM
 *  @{ */
/*****************************************************************************/
/** Open load detection by motor current measurement.
    If current is below defined threshold, open load is assumed.
		Set threshold to 0 mA to deactivate open load detection.
		Threshold in [mA]
		Max. 65535
*/
#define MSM_OPEN_LOAD_CURRENT_THRESHOLD_MA				(2U)

/** Open load is detected if motor current is below threshold for 
    the defined number of motor state machine cycles.
		Max. 65535
*/
#define MSM_OPEN_LOAD_DURATION_CYCLES							(10U)

/** Stall detection threshold in percent of maximum motor current.
    If current is above defined threshold, stall is assumed.
    Maximum current calculated as:
		I-max [mA] = (V-BVDD/(motor-resistance + 2 x MOUT-RDSon)) x PWM-ton/toff
		Max. 65535
*/
#define MSM_STALL_CURRENT_THRESHOLD_PC						(80U)

/** Stall is detected if motor current is above threshold for 
    the defined number of motor state machine cycles.
		Max. 65535
*/
#define MSM_STALL_DURATION_CYCLES									(5U)

/** Stall detection and open load detection is blanked out for defined number of
    MSM motor state machine cycles.
		Allows the ADC moving averagers to fill up.
		Max. 65535
*/
#define MSM_OPEN_STALL_BLANK_CYCLES								(10U)

/** @}		end of group Module-MSM */


/*****************************************************************************/
/** @addtogroup Module-MOT
 *  @{ */
/*****************************************************************************/
/** Softstart decimation default value. 
    max. 65535
		EPWM compare value increment/decrement per no. of PWM periods.
		E.g. MOT_SOFTSTART_DECIMATION defined as 50
		     Actual EPWM compare value is 0
				 Requested EPWM compare value is 500
				 With a compare value increment every 50 EPWM periods, the requested 
				 value is reached after 50*500 = 25000 EPWM periods (= 1.25s at 20 kHz)
*/
#define MOT_SOFTSTART_DECIMATION		(50U)


/** Motor control mode.

  Setting | FRWHL and Decay
	--------|-----------------
	0       | BDC connected to MOUT0/1 and MOUT2/3
	1       | BDC connected to external FETs
*/
#define MOT_BRIDGE_SELECT	1//(0U)

/** Terminal to terminal resistance of motor one [mOhm ]
    (temperature drift compensation of motor resistance not implemented)
*/

#if(MOT_BRIDGE_SELECT==0)
#define MOT_MOTOR_RESISTANCE_MO							(26000U)
#else
#define MOT_MOTOR_RESISTANCE_MO							(3000U)
#endif

/**  2 x MOUT RDSon in mOhm 
*/
#if(MOT_BRIDGE_SELECT==0)
#define MOT_MOUT_HL_RDSON_MO 					(5000U)
#else
#define MOT_MOUT_HL_RDSON_MO 					(45U)
#endif

/** EPWM alignment mode. 

	0 = center
	
	1 = edge 
*/
#define MOT_EPWM_PAM							(0U)

/** Selectes the EPWM module input clock.

  fEPWM_IN = fsys/2^fsel 
*/
#define MOT_EPWM_FSEL							(0U)

/** EPWM period compare setting.

  Defines the EPWM output frequency.
  E.g. in center aligned mode fPWM = fEPWM_IN / (2*EPWM_PERx) 
*/
#define MOT_EPWM_PERIOD_COMPARE		(500U)

/** MOUT slew rate control.
 
  Setting | Slew rate
	--------|-----------------
	0       | normal
	1       | fast

*/
#define MOT_EPWM_SR			(1U)

/** MOUT over-current filter time.

 Setting | time [us]
 --------------------
 0       | 2.4 to 2.8
 1       | 4.8 to 5.6
 2       | 9.6 to 11.2
 3       | 19.2 to 22.4
*/
#define MOT_EPWM_OCFT			(0U)

/** MOUT Cross-current protection time.

 time = CCPT * 200 ns

 Setting | time [ns]
 --------------------
 0       | 50
 1       | 200
 2       | 400
 3       | 600
 4       | 800
 5       | 1000
 6       | 1200
 7       | 1400
 8       | 1600
 9       | 1800
 10      | 2000
 11      | 2200
 12      | 2400
 13      | 2600
 14      | 2800
 15      | 3000
*/
#define MOT_EPWM_CCPT			(13U)


/** MOUT freewheeling and decay setting.

  Setting | FRWHL and Decay
	--------|-----------------
	0       | passive/asynchronous low-side freewheeling, slow decay (high-side PWM)
	1       | active/synchronous low-side freewheeling, slow decay (complementary PWM, active high)
	2       | active/synchronous high-side freewheeling, slow decay (complementary PWM, active low)
*/
#define MOT_FRWHL					(0U)

/** @}		end of group Module-MOT */


/**
 *  @}		end of group Configuration-Header
 */


#endif		/* __CONFIG_H */

