
#ifndef __EEPROM_H
#define __EEPROM_H
//-----------------------------------------------------------------------------

#include "config.h"

//-----------------------------------------------------------------------------
// Definitions
//-----------------------------------------------------------------------------

#define ULA_EEPROM (0x1F7C5923)
#define ULB_EEPROM (0xE083A6DC)

#define CTRL_W_EEPROM				0x12345678		// control word for writing to the EEPROM
#define CTRL_E_EEPROM				0x87654321		// control word for erasing an EEPROM sector

//-----------------------------------------------------------------------------
// Function Prototypes
//-----------------------------------------------------------------------------
unsigned int WRITE_EEPROM_WORD(unsigned int addr, unsigned int data, unsigned int ctrl, unsigned int cmd);
unsigned int ERASE_EEPROM_SECTOR(unsigned int addr, unsigned int ctrl, unsigned int cmd);
unsigned int EEPROM_WRITE(void);
void EEPROM_DATA_READ(void);
#endif /* #ifndef __EEPROM_H */


