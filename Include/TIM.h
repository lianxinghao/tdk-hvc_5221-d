
#ifndef __TIM_H
#define __TIM_H

#include "config.h"

#define TIM0_AR 99
#define SPEED_CAL_TIME 10000000/(TIM0_AR+1)
#define Min_time 10000/(TIM0_AR+1)
extern int TIM0_Handler_Counter;
extern int TIM1_Handler_Counter;

void TIM_Init(void);

#endif /* #ifndef __TIM_H */


