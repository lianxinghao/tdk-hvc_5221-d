
#ifndef __MAIN_H
#define __MAIN_H

#include "ADC.h"
#include "SYSCTRL.h"
#include "TIM.h"
#include "EPWM.h"
#include "LIN.h"
#include "SEGGER_RTT.h"
#include "BEMFC.h"
#include "Variable.h"
#include "Task.h"
#include "EEPROM.h"

extern int Close_Step;
extern int Time;
extern int Last_Save_Beat;
extern double RPM_Speed;

#endif		/* __MAIN_H */

