

#include "SYSCTRL.h"


void SYSCTRL_Init(void){
	WWDG_Stop();
	//EMI降低模块，降低可能对其他电子设备的干扰电磁辐射
	SYSCTRL->ERMCR_b.EN = ERM_ENABLE;	/* enable/disable ERM */
	SYSCTRL->CPUDIV_b.CPUDIV=0;
	SYSCTRL->PCR_b.GTS=0;		//0：正常模式1：进入休眠模式
	
}
