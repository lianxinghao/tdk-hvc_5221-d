#include "LIN.h"



void LIN_Init(void){
	
	/****************LIN****************/
	LIN->CR_b.EN=1;//Enable LIN port
	LIN->CR_b.RXFT=4;//RX Filter Time
	/*
	RXFT[2:0] RX Filter Time [μs] 1)
	min max
	0 Input filter bypassed
	1 0.3 0.35
	2 0.6 0.7
	3 1.2 1.4
	4 2.4 2.8
	5 4.8 5.6
	6 9.6 11.2
	7 19.2 22.4
	*/
	LIN->CR_b.OCFT=0;
	/*
	OCFT[1:0] Over-current Filter Time [μs]1)
	min max
	0 2.4 2.8
	1 4.8 5.6
	2 9.6 11.2
	3 19.2 22.4
	*/
	LIN->CR_b.LAIS=0;//Routed to LIN RX
	LIN->CR_b.TXOS=0;;//Routed to LINUART_TX
	/*
	TXOS[1:0] LIN_TX Output
	0 Routed to LINUART_TX
	1 Routed to alternative output TIMER0_OUT
	2 Routed to alternative output TIMER1_OUT
	3 Routed to LIN data output register LIN_DO
	*/
	LIN->CR_b.SR=2;
	/*
	SR[1:0] LIN Slew Rate Remark
	0 maximum		Unlimited slope (slope control off)
	1 1.25 V/μs 	Limited slope (slope control on)
	2 1.5 V/μs		Settings applicable with LIN baud rate of 10 kBit/s or 20 kBit/s. 
	3 1.75 V/μs
	*/
	LIN->CR_b.DOMTOEN=0;// TX Dominant Timeout Detection Enable
	LIN->CR_b.LOM=0;// LIN Port Operating Mode
	LIN->CR_b.TAIS0=0;//TIMER 0/1 Alternative LIN Input Select [x = 0,1]
	LIN->CR_b.TAIS1=0;
	
	LIN->IEN_b.BEND=1;
	
	NVIC_SetPriority(LIN_IRQn, LIN_IRQ_PRIO);
	LIN->EIPND=0x0F;
	NVIC_EnableIRQ(LIN_IRQn);			/* enable in NVIC */
	
	
	
	
	
	/****************LINUART****************/
	
	
	
	LINUART->CR_b.TXRXEN=1;
	LINUART->CR_b.AUTEN=1;
	
	LINUART->CR_b.LINEN=1;
	LINUART->CR_b.ISTVEN=1;
	LINUART->CR_b.RXDIS=0;
	LINUART->CR_b.TXMODE=0;//Start transmission by writing the TXD register only
	LINUART->CR_b.STP=0;//2位停止位
	LINUART->CR_b.PAR=0;
	LINUART->CR_b.PAREN=0;//无校验位
	LINUART->CR_b.MSMP=0;//
	LINUART->CR_b.TXFCV=0;
	LINUART->CR_b.RXFCV=0;
	LINUART->CR_b.TINV=0;
	LINUART->CR_b.RINV=0;
	LINUART->CR_b.LLB=0;//Local Loop Back 本地回环，测试用途
	LINUART->IEN_b.RXFC=1;
	LINUART->IEN_b.TXFC=1;
	LINUART->IEN_b.TXEND=1;
	LINUART->EIPND = 0xFFFF;
	LINUART->CR_b.EN=1;
	NVIC_SetPriority(LINUART_IRQn, LINUART_IRQ_PRIO);
	NVIC->ISER[0]	= 1<<LINUART_IRQn; 	// enable LINUART interrupt line(s)
	
}
char recv;
char send='a';
void LIN_FUNCTION(void)
{
	Write_TXD_Register(send);
	recv=Receive_RXD_Register();
	
}
void Write_TXD_Register(char Data){
	LINUART->TXD_b.DATA=Data;
}

char Receive_RXD_Register(void){
	return LINUART->RXD_b.DATA;
}



int LIN_Handler_Counter=0;
void LIN_IRQHandler(void){
	u32 pending_u32;
	pending_u32 = LIN->EIPND&0x000000FF;		/* read enabled interrupts pending flag EPWM0  */
	LIN->EIPND = pending_u32;				/* clear enabled interrupts pending flags */
	if(pending_u32&0x00000010){
		LIN_Handler_Counter++;
	}
}


int LINUART_RX_Handler_Counter=0;
int LINUART_TX_Handler_Counter=0;
int LINUART_TXEND_Handler_Counter=0;

void LINUART_IRQHandler(void)
{
	u32 pending_u32;
	pending_u32 = LINUART->EIPND&0x0000FFFF;		/* read enabled interrupts pending flag EPWM0  */
	LINUART->EIPND = pending_u32;				/* clear enabled interrupts pending flags */
	
	if(pending_u32&0x00000040)				/* RXFC */
	{	
		LINUART_RX_Handler_Counter++;
	}	
	if(pending_u32&0x00000004)				/* TXFC */
	{	
		LINUART_TX_Handler_Counter++;
	}	
	if(pending_u32&0x00001000)				/* TXFC */
	{	
		LINUART_TXEND_Handler_Counter++;
	}	
	
}
